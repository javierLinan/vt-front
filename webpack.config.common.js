var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
// var webpack = require('webpack');

module.exports = {
  resolve: {
    extensions: ['', '.js'],
    fallback: [path.join(__dirname, 'node_modules')],
    alias: {
      'src': path.resolve(__dirname, 'src'),
      'test': path.resolve(__dirname, 'test'),
      'js': path.resolve(__dirname, 'src/js'),
      'partials': path.resolve(__dirname, 'src/partials'),
    }
  },
  module: {
    preLoaders: [],
    loaders: [
      {
        test: /\.js$/,
        loader: 'strict',
        exclude: [
          path.resolve(__dirname, 'node_modules'),
          path.resolve(__dirname, 'assets')
        ],
      },
      {
        test: /\.html$/,
        loader: 'ngtemplate?relativeTo=' + (path.resolve(__dirname, '/src')) + '/!html',
        exclude: [
          path.resolve(__dirname, 'node_modules'),
          path.resolve(__dirname, 'assets')
        ],
      },
      // {
      //   test: /\.html$/,
      //   loader: 'raw',
      //   exclude: [
      //     path.resolve(__dirname, 'node_modules'),
      //     path.resolve(__dirname, 'assets')
      //   ],
      // }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'assets/index.tpl.html',
      inject: 'body',
      hash: 'true',
    }),
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: 'vendor',
    //   minChunks: function (module, count) {
    //     return module.resource && module.resource.indexOf(path.resolve(__dirname, 'src')) === -1;
    //   }
    // })
  ]
};
