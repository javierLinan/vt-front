var resolvers = {
  terms: function($q, $timeout, ApiService) {
    var deferred = $q.defer();
    $timeout(function() {
      deferred.resolve(ApiService.getTerms());
    }, 1000);
    return deferred.promise;
  },
  term: function($stateParams, ApiService) {
    return ApiService.getTerm($stateParams.termId);
  }
};

var states = [
  {
    name: 'home',
    url: '/',
    templateUrl: 'partials/home_page.html',
    resolve: {
      terms: resolvers.terms
    },
    controller: 'Home'
  },
  {
    name: 'termNew',
    url: '/term/new',
    templateUrl: 'partials/term/term_new_page.html',
    controller: 'TermNew'
  },
  {
    name: 'termDetail',
    url: '/term/:termId',
    templateUrl: 'partials/term/term_detail_page.html',
    resolve: {
      term: resolvers.term
    },
    controller: 'TermDetail'
  },
];

var vtApp = angular.module('vt', ['ui.router', 'ngMessages', 'vt.controllers', 'vt.services'])
  .config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    states.forEach(function(state) {
      $stateProvider.state(state);
    });
  });
