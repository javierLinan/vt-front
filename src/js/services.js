// var angular = require('angular');

function getTerms(ApiEndpoint, $http) {
  return $http({
    url: ApiEndpoint.serverUrl + ApiEndpoint.baseUrl + 'terms',
    method: 'GET',
    headers: ApiEndpoint.headers(),
  }).then(function(response) {
    return response.data.terms;
  });
}

function createTerm(ApiEndpoint, $http, term) {
  return $http({
    url: ApiEndpoint.serverUrl + ApiEndpoint.baseUrl + 'terms',
    method: 'POST',
    headers: ApiEndpoint.headers(),
    data: term
  });
}

function createTranslation(ApiEndpoint, $http, translation, term) {
  return $http({
    url: ApiEndpoint.serverUrl + ApiEndpoint.baseUrl + 'terms/' + term.id + '/translations',
    method: 'POST',
    headers: ApiEndpoint.headers(),
    data: translation
  }).then(function(response){
    return response.data.translation;
  });
}

function createTermAndTranslation(ApiEndpoint, $http, termAndTranslation) {
  return $http({
    url: ApiEndpoint.serverUrl + ApiEndpoint.baseUrl + 'terms/translations',
    method: 'POST',
    headers: ApiEndpoint.headers(),
    data: termAndTranslation
  }).then(function(response){
    return {
      'term': response.data.term,
      'translation': response.data.translation,
    };
  });
}

function updateTranslation(ApiEndpoint, $http, translation) {
  return $http({
    url: ApiEndpoint.serverUrl + ApiEndpoint.baseUrl + 'translations/' + translation.id,
    method: 'PUT',
    headers: ApiEndpoint.headers(),
    data: {
      translation: translation.translation,
      type: translation.type,
      example: translation.example,
    }
  }).then(function(response){
    return response.data.translation;
  });
}

function removeTranslation(ApiEndpoint, $http, translation) {
  return $http({
    url: ApiEndpoint.serverUrl + ApiEndpoint.baseUrl + 'translations/' + translation.id,
    method: 'DELETE',
    headers: ApiEndpoint.headers()
  }).then(function(response){
    return response.data.translation_id;
  });
}

function getTerm(ApiEndpoint, $http, termId) {
  return $http({
    url: ApiEndpoint.serverUrl + ApiEndpoint.baseUrl + 'terms/' + termId ,
    method: 'GET',
    headers: ApiEndpoint.headers(),
  }).then(function(response) {
    return response.data.term;
  });
}

angular.module('vt.services', [])
  .service('ApiService', function($http, $httpParamSerializerJQLike, ApiEndpoint) {
    var self = this;
    return {
      getTerms: angular.bind(self, getTerms, ApiEndpoint, $http),
      createTerm: angular.bind(self, createTerm, ApiEndpoint, $http),
      getTerm: angular.bind(self, getTerm, ApiEndpoint, $http),
      createTranslation: angular.bind(self, createTranslation, ApiEndpoint, $http),
      updateTranslation: angular.bind(self, updateTranslation, ApiEndpoint, $http),
      removeTranslation: angular.bind(self, removeTranslation, ApiEndpoint, $http),
      createTermAndTranslation: angular.bind(self, createTermAndTranslation, ApiEndpoint, $http),
    };
  });
