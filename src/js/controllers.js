function createTerm(term, ApiService, successCallback, errorCallback, termForm) {
  termForm.$setSubmitted();

  if(termForm.$valid) {
    ApiService.createTerm(angular.copy(term)).then(successCallback, errorCallback);
  }
}

function createTranslation(ApiService, successCallback, errorCallback, newTranslation, term, translationForm) {
  translationForm.$setSubmitted();

  if(translationForm.$valid) {
    ApiService.createTranslation(angular.copy(newTranslation), term).then(successCallback, errorCallback);
    translationForm.$setPristine();
    cleanObject(newTranslation);
  }
}

function updateTranslation(ApiService, successCallback, errorCallback, translation, translationForm) {
  var id = translation.id;
  translationForm.$setSubmitted();

  if(translationForm['type_' + id].$valid &&
    translationForm['translation_' + id].$valid &&
    translationForm['example_' + id].$valid) {
    ApiService.updateTranslation(translation).then(successCallback, errorCallback);
    translationForm.$setPristine();
  }
}

function removeTranslation(ApiService, successCallback, errorCallback, translation) {
  ApiService.removeTranslation(translation).then(successCallback, errorCallback);
}

function createTermAndTranslation(termAndTranslation, ApiService, successCallback, errorCallback, termAndTranslationForm) {
  termAndTranslationForm.$setSubmitted();

  if(termAndTranslationForm.$valid) {
    ApiService.createTermAndTranslation(angular.copy(termAndTranslation)).then(successCallback, errorCallback);
    termAndTranslationForm.$setUntouched();
    cleanObject(termAndTranslation);
  }
}

function postCreateTranslation(translations, translationsShadow, createdTranslation) {
  translationsShadow.push(createdTranslation);
  translations.push(createdTranslation);
}

function postUpdateTranslation(translations, statesUpdating, updatedTranslation) {
  //We re inversing the behaviour of the function
  notify('Updated the translation ' + updatedTranslation.translation);
  toggleUpdate([updatedTranslation], translations, statesUpdating, updatedTranslation.id);
}

function postRemoveTranslation(translations, translationsShadow, removedTranslationId) {
  var position = translations.map(function(item) {
    return item.id+'';
  }).indexOf(removedTranslationId);

  if(position != -1) {
    translations.splice(position, 1);
    translationsShadow.splice(position, 1);
  }
}

function postCreateTermTranslation(terms, termAndTranslation) {
  terms.push(termAndTranslation.term);
}

function toggleUpdate(translations, translationsShadow, statesUpdating, id) {
  var translation;
  var shadowTranslation;

  function findFunction(item) {
    return item.id == id;
  }

  statesUpdating[id] = !statesUpdating[id];

  //If transition from open to closed, we need to update the values.
  if(!statesUpdating[id]) {
    translation = translations.find(findFunction);
    shadowTranslation = translationsShadow.find(findFunction);

    shadowTranslation.type = translation.type;
    shadowTranslation.translation = translation.translation;
    shadowTranslation.example = translation.example;
  }
}

function notify(message) {

}

function cleanObject(object) {
  Object.keys(object).map(function(key) {
    if(angular.isObject(object[key])) {
      object[key] = cleanObject(object[key]);
    } else {
      object[key] = '';
    }
  });
  return object;
}

function navigateState(state, stateProvider) {
  stateProvider.go(state);
}

angular.module('vt.controllers', [])
  .controller('Home', function($scope, terms, ApiService){
    var self = this;
    var termAndTranslation = {};
    var successCreateCallback = angular.bind(self, postCreateTermTranslation, terms);

    $scope.terms = terms || [];
    $scope.termAndTranslation = termAndTranslation;
    $scope.createTermAndTranslation = angular.bind(self, createTermAndTranslation, termAndTranslation, ApiService, successCreateCallback, function(response){});
  })
  .controller('TermNew', function($scope, $state, ApiService){
    var self = this;
    var term = {};
    var successCreateCallback = angular.bind(self, navigateState, 'termList', $state);

    $scope.term = term;
    $scope.createTerm = angular.bind(self, createTerm, term, ApiService, successCreateCallback, function(response){});
  })
  .controller('TermDetail', function($scope, $state, ApiService, term){
    var self = this;
    var newTranslation = {};
    var statesUpdating = {};
    var statesRemoving = {};
    var translations = term.translations;
    var translationsShadow = angular.copy(translations);
    var successCreateCallback = angular.bind(self, postCreateTranslation, translations, translationsShadow);
    var successUpdateCallback = angular.bind(self, postUpdateTranslation, translations, statesUpdating);
    var successRemoveCallback = angular.bind(self, postRemoveTranslation, translations, translationsShadow);

    $scope.newTranslation = newTranslation;
    $scope.term = term || {};
    $scope.statesUpdating = statesUpdating;
    $scope.statesRemoving = statesRemoving;
    $scope.translations = translations || [];
    $scope.translationsShadow = translationsShadow;
    $scope.rows = [];

    for(var key in term.translations) {
      statesUpdating[term.translations[key].id] = false;
      statesRemoving[term.translations[key].id] = false;
    }

    $scope.createTranslation = angular.bind(self, createTranslation, ApiService, successCreateCallback, function(response) {});
    $scope.updateTranslation = angular.bind(self, updateTranslation, ApiService, successUpdateCallback, function(response) {});
    $scope.removeTranslation = angular.bind(self, removeTranslation, ApiService, successRemoveCallback, function(response) {});
    $scope.toggleUpdate = angular.bind(self, toggleUpdate, term.translations, translationsShadow, statesUpdating);
    $scope.toggleRemove = function(id) {
      statesRemoving[id] = !statesRemoving[id];
    };
  });
