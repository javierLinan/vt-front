//Angular libs
require('angular');
require('angular-messages');
require('angular-ui-router');

//Templates are cached
require('partials/home_page.html');
require('partials/error_list.html');
require('partials/term_translation/term_translation_new.html');
require('partials/term/term_list.html');
require('partials/term/term_detail_page.html');
require('partials/term/term_new_page.html');

//App libs
require('js/controllers');
require('js/services');
require('js/variables');
require('js/app.js');
