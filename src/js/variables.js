// var angular = require('angular');

angular.module('vt.services')
  .factory('ApiEndpoint', function (){
    return {
      baseUrl: '/api/v1/',
      serverUrl: 'http://ws.vt-server/app_dev.php',

      headers: function () {
        var headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        };

        return headers;
      }
    };
  });
